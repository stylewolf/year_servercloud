package com.zx.year.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.zx.framework.core.pagination.Pagination;
import com.zx.framework.web.controller.BaseController;
import com.zx.year.po.User;
import com.zx.year.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController extends BaseController {

    private static final Logger LOGGER = Logger.getLogger(UserController.class);
    @Resource
    private UserService userService;

    @InitBinder("user")
    public void userBinder(WebDataBinder binder) throws Exception {
        binder.setFieldDefaultPrefix("user.");
    }

    
    
    /*附加的其他业务跳转*/
}
