package com.zx.year.po;

import java.util.List;

import com.zx.framework.core.util.RandomUtil;

public class Config {
	/**
	 * 未抽奖显示信息
	 */
	public static  String[] messages = new String[]{"还没开始抽奖，请不要着急","真的没开始抽奖呢","恭喜您，抽奖还未开始","亲，别着急，还木有开始抽奖呢！"};
	/**
	 * 还未开始评定优秀员工
	 */
	public static String[] yxMessages = new String[]{"还没开始公布，请耐心等待","亲，别着急！"};
	
	public static String randomMessage(){
		List<Integer> s = RandomUtil.getRandom(messages.length,1);
		return messages[s.get(0)];
	}
	
	public static String randomYxMessage(){
		List<Integer> s = RandomUtil.getRandom(yxMessages.length,1);
		return yxMessages[s.get(0)];
	}
	
	public static void main(String[] args) {
		System.out.println(randomMessage());
	}
}
