package com.zx.year.po;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Message entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "y_message", catalog = "year")
public class Message implements java.io.Serializable {

	// Fields

	private String id;
	private String userid;
	private String message;
	private Integer type;
	private Long createtime;

	// Constructors

	/** default constructor */
	public Message() {
	}

	/** minimal constructor */
	public Message(String id, String userid, String message, Integer type) {
		this.id = id;
		this.userid = userid;
		this.message = message;
		this.type = type;
	}

	/** full constructor */
	public Message(String id, String userid, String message, Integer type,
			Long createtime) {
		this.id = id;
		this.userid = userid;
		this.message = message;
		this.type = type;
		this.createtime = createtime;
	}

	// Property accessors
	@Id
	@Column(name = "ID", unique = true, nullable = false, length = 64)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "USERID", nullable = false, length = 64)
	public String getUserid() {
		return this.userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	@Column(name = "MESSAGE", nullable = false, length = 400)
	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Column(name = "TYPE", nullable = false, precision = 1, scale = 0)
	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name = "CREATETIME", precision = 13, scale = 0)
	public Long getCreatetime() {
		return this.createtime;
	}

	public void setCreatetime(Long createtime) {
		this.createtime = createtime;
	}

}