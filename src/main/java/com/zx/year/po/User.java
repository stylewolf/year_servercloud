package com.zx.year.po;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * User entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "y_user", catalog = "year")
public class User implements java.io.Serializable {

	// Fields

	private String id;
	private String name;
	private Integer cj;
	private Integer qd;
	private Integer j1;
	private Integer j2;
	private Integer j3;
	private Long qdsj;
	private String dept;
	private Integer yx;

	// Constructors

	/** default constructor */
	public User() {
	}

	/** minimal constructor */
	public User(String id, String name, Integer cj) {
		this.id = id;
		this.name = name;
		this.cj = cj;
	}

	/** full constructor */
	public User(String id, String name, Integer cj, Integer qd, Integer j1,
			Integer j2, Integer j3, Long qdsj, String dept, Integer yx) {
		this.id = id;
		this.name = name;
		this.cj = cj;
		this.qd = qd;
		this.j1 = j1;
		this.j2 = j2;
		this.j3 = j3;
		this.qdsj = qdsj;
		this.dept = dept;
		this.yx = yx;
	}

	// Property accessors
	@Id
	@Column(name = "ID", unique = true, nullable = false, length = 20)
	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Column(name = "NAME", nullable = false, length = 20)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "CJ", nullable = false, precision = 1, scale = 0)
	public Integer getCj() {
		return this.cj;
	}

	public void setCj(Integer cj) {
		this.cj = cj;
	}

	@Column(name = "QD", precision = 1, scale = 0)
	public Integer getQd() {
		return this.qd;
	}

	public void setQd(Integer qd) {
		this.qd = qd;
	}

	@Column(name = "J1", precision = 1, scale = 0)
	public Integer getJ1() {
		return this.j1;
	}

	public void setJ1(Integer j1) {
		this.j1 = j1;
	}

	@Column(name = "J2", precision = 1, scale = 0)
	public Integer getJ2() {
		return this.j2;
	}

	public void setJ2(Integer j2) {
		this.j2 = j2;
	}

	@Column(name = "J3", precision = 1, scale = 0)
	public Integer getJ3() {
		return this.j3;
	}

	public void setJ3(Integer j3) {
		this.j3 = j3;
	}

	@Column(name = "QDSJ", precision = 13, scale = 0)
	public Long getQdsj() {
		return this.qdsj;
	}

	public void setQdsj(Long qdsj) {
		this.qdsj = qdsj;
	}

	@Column(name = "DEPT", length = 64)
	public String getDept() {
		return this.dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	@Column(name = "YX", precision = 1, scale = 0)
	public Integer getYx() {
		return this.yx;
	}

	public void setYx(Integer yx) {
		this.yx = yx;
	}

}