package com.zx.year.service;

import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.zx.framework.core.exception.ServiceException;
import com.zx.framework.core.pagination.Pagination;
import com.zx.framework.db.IBaseDAO;
import com.zx.framework.db.KeyValue;
import com.zx.framework.db.exception.DatabaseException;
import com.zx.year.po.Message;

@Component("messageService")
@Transactional
public class MessageService {
	private static final Logger LOGGER = Logger.getLogger(MessageService.class);
	@Resource
	private IBaseDAO baseDAO;
	
	/**
	 * 获取|Message|详细信息
	 * @param id
	 * @return
	 */
	public Message findMessageById(java.lang.String id){
		if(id==null){
			ServiceException exception = new ServiceException("|Message|id不能为空!");
			LOGGER.error(exception.getMessage());
			throw exception;
		}
		try {
			return baseDAO.findById(id, Message.class);
		}catch (Exception e) {
			LOGGER.error("获取Message失败！",e);
			throw new DatabaseException("获取|Message|失败！");
		}
	}
	
	/**
	 * 保存|Message|详细信息
	 * @param dept
	 * @return
	 */
	public void saveMessage(Message message){
		if(message==null){
			ServiceException exception = new ServiceException("保存|Message|信息不能为空!");
			LOGGER.error(exception.getMessage());
			throw exception;
		}
		try {
			baseDAO.save(message);
		}catch (Exception e) {
			LOGGER.error("保存|Message|失败！",e);
			throw new DatabaseException("保存|Message|失败！");
		}
	}
	
	/**
	 * 更新|Message|详细信息
	 * @param dept
	 * @return
	 */
	public void updateMessage(Message message){
		if(message==null){
			ServiceException exception = new ServiceException("更新|Message|信息不能为空!");
			LOGGER.error(exception.getMessage());
			throw exception;
		}
		try {
			baseDAO.update(message);
		}catch (Exception e) {
			LOGGER.error("修改|Message|失败！",e);
			throw new DatabaseException("修改|Message|失败！");
		}
	}
	
	/**
	 * 删除|Message|详细信息
	 * @param message
	 * @return
	 */
	public void removeMessage(Message message){
		if(message==null){
			ServiceException exception = new ServiceException("删除|Message|信息不能为空!");
			LOGGER.error(exception.getMessage());
			throw exception;
		}
		try {
			baseDAO.remove(message);
		}catch (Exception e) {
			LOGGER.error("删除|Message|失败！",e);
			throw new DatabaseException("删除|Message|失败！");
		}
	}
	
	/**
	 * 分页查询|Message|列表信息
	 * @param example
	 * @param page
	 * @param rows
	 * @return
	 */
	public Pagination<Message> pageQuery(Message example,int page,int rows){
		String qryHql = "from com.zx.year.po.Message";
		String cntHql = "select count(*) from com.zx.year.po.Message";
		//条件查询可以放在这里 
		if(example!=null){
			//to-do
		}
		try {
			return baseDAO.hqlPageQuery(qryHql, cntHql, page, rows);
		}catch (Exception e) {
			LOGGER.error("查询|Message|列表失败！",e);
			throw new DatabaseException("查询|Message|列表失败！");
		}
	}
	/**
	 * 获取分页对象
	 * @param qrySql
	 * @param cntSql
	 * @param start
	 * @param size
	 * @param params
	 * @return
	 */
	public Pagination sqlPageQuery(String qrySql, String cntSql,
			int current, int size, Object... params) {
		return baseDAO.sqlPageQuery(qrySql, cntSql, current, size, params);
	}
	/**
	 * 执行sql语句
	 * @param sql
	 * @param params
	 * @return 记录变更条数
	 */
	public int executeSQL(String sql, Object... params) {
		try {
			return baseDAO.executeSQL(sql, params);
		}catch (Exception e) {
			LOGGER.error("执行|Message| SQL更新失败！",e);
			throw new DatabaseException("执行|Message|更新失败！");
		}
	}
	
	/**
	 * 执行hql语句
	 * @param hql
	 * @param params
	 * @return 记录变更条数
	 */
	public int executeHQL(String hql, Object... params) {
		try {
			return baseDAO.executeHQL(hql, params);
		}catch (Exception e) {
			LOGGER.error("执行|Message| HQL更新失败！",e);
			throw new DatabaseException("执行|Message|更新失败！");
		}
	}
	
	/**
	 * 获取sql查询列表，相当于start和size不满足判断
	 * @param sql
	 * @param params 占位参数
	 * @return java.util.Map
	 */
	public List querySQL(String sql, Object... params) {
		return baseDAO.querySQL(sql, params);
	}
	/**
	 * 获取hql查询列表
	 * @param hql
	 * @param params
	 * @return
	 */
	public List queryHQL(String hql,Object...params){
		return baseDAO.queryHQL(hql, params);
	}
	
	/**
	 * 根据属性值返回|Message|列表
	 * @param propertyName
	 * @param value
	 * @param orders 排序的字段名称及升降序
	 * @return
	 */
	public List findByProperty(String propertyName,
			Object value, String... orders) {
		return baseDAO.findByProperty("com.zx.year.po.Message", propertyName, value, orders);
	}

	/**
	 * 根据属性值返回|Message|列表
	 * @param queryParam
	 * @param orders 排序的字段名称及升降序
	 * @return
	 */
	public List findByProperty(KeyValue queryParam,
			String... orders) {		
		return baseDAO.findByProperty("com.zx.year.po.Message", queryParam, orders);
	}

	/**
	 * 根据属性值返回|Message|列表
	 * @param propertyName
	 * @param value
	 * @return
	 */
	public List findByProperty( String propertyName,
			Object value) {
		return baseDAO.findByProperty("com.zx.year.po.Message", propertyName, value);
	}

	/**
	 * 根据属性值返回|Message|列表
	 * @param queryParam
	 * @return
	 */
	public List findByProperty(KeyValue queryParam) {
		return baseDAO.findByProperty("com.zx.year.po.Message", queryParam);
	}

	/**
	 * 查找属性propertyNames值为values的数据， 所有条件求取并集，
	 * 并且按照orders进行排序，如列表中保存两项：“id desc”，“name”
	 * 表必须在hibernate中声明，返回列表为hibernate声明结构类。
	 * @param queryParams
	 * @param start
	 * @param size
	 * @param orders
	 * @return
	 */
	public List findByPropertys( List<KeyValue> queryParams,
			int start, int size, String... orders) {
		return baseDAO.findByPropertys("com.zx.year.po.Message", queryParams, start, size, orders);
	}

	/**
	 * 查找属性propertyNames值为values的数据， 所有条件求取并集，
	 * 并且按照orders进行排序，如列表中保存两项：“id desc”，“name”
	 * 表必须在hibernate中声明，返回列表为hibernate声明结构类。
	 * @param tableName
	 * @param queryParams
	 * @param orders
	 * @return
	 */
	public List findByPropertys(List<KeyValue> queryParams,
			String... orders) {
		return baseDAO.findByPropertys("com.zx.year.po.Message", queryParams, orders);
	}

	/**
	 * 返回所有|Message|
	 * @return
	 */
	public List findAll() {
		return baseDAO.findAll("com.zx.year.po.Message");
	}

	/**
	 * 返回排序的所有|Message|
	 * @param orders
	 * @return
	 */
	public List findAll(String... orders) {
		return baseDAO.findAll("com.zx.year.po.Message", orders);
	}

	/**
	 * 返回单条统计函数值 例如count 或者确实只有一条记录返回的时候
	 * @param <T>
	 * @param hql
	 * @param type
	 * @return
	 */
	public <T> T getHQLUnique(String hql, Class<T> type, Object... params) {
		return baseDAO.getHQLUnique(hql, type, params);
	}

	/**
	 * 返回单条统计函数值 例如count size 或者确实只有一条记录返回的时候
	 * @param <T>
	 * @param sql
	 * @param type
	 * @return
	 */
	public <T> T getSQLUnique(String sql, Class<T> type, Object... params) {
		return baseDAO.getSQLUnique(sql, type, params);
	}
	
	/////自定义的service方法
	
}
